package com.ustore.sso.util;

public class TestObjects {

	public static final String TEST_USER = "{\"name\":\"User test\",\"email\":\"test@example.com\",\"password\":\"test123\"}";

	public static final String TEST_USER_LOGIN = "{\"email\":\"test@example.com\",\"password\":\"test123\"}";
	public static final String TEST_USER_LOGIN_WRONG_PW = "{\"email\":\"test@example.com\",\"password\":\"123456\"}";
	
	public static final String FAKE_TOKEN = "{\"token\":\"1234567890\"}";
}
