package com.ustore.sso;

import static org.hamcrest.CoreMatchers.isA;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import com.ustore.sso.util.TestObjects;

@SpringBootTest
@AutoConfigureMockMvc
@DirtiesContext(classMode = ClassMode.BEFORE_EACH_TEST_METHOD)
@AutoConfigureTestDatabase(replace = Replace.ANY)
public class SessionTests {

	@Autowired
	private MockMvc mockMvc;
	
	@Test
	public void loginUser() throws Exception {
		
		this.mockMvc.perform(post("/user/register")
				.content(TestObjects.TEST_USER)
				.header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON))
		.andDo(print())
		.andExpect(status().isOk());
		
		this.mockMvc.perform(post("/session/login")
				.content(TestObjects.TEST_USER_LOGIN)
				.header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON))
		.andDo(print())
		.andExpect(status().isOk())
		.andExpect(jsonPath("$.token", isA(String.class)))
		.andExpect(jsonPath("$.id").doesNotExist());
	}
	
	@Test
	public void wrongPwLogin() throws Exception {
		this.mockMvc.perform(post("/user/register")
				.content(TestObjects.TEST_USER)
				.header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON))
		.andDo(print())
		.andExpect(status().isOk());
		
		this.mockMvc.perform(post("/session/login")
				.content(TestObjects.TEST_USER_LOGIN_WRONG_PW)
				.header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON))
		.andDo(print())
		.andExpect(status().isBadRequest());
	}
	
	@Test
	public void unregisterdUserLogin() throws Exception {
		this.mockMvc.perform(post("/session/login")
				.content(TestObjects.TEST_USER_LOGIN)
				.header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON))
		.andDo(print())
		.andExpect(status().isBadRequest());
	}
	

	@Test
	public void logoutUser() throws Exception {
		
		this.mockMvc.perform(post("/user/register")
				.content(TestObjects.TEST_USER)
				.header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON))
		.andDo(print())
		.andExpect(status().isOk());
		
		MvcResult result = this.mockMvc.perform(post("/session/login")
				.content(TestObjects.TEST_USER_LOGIN)
				.header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON))
		.andDo(print())
		.andExpect(status().isOk())
		.andReturn();
		
		String token = result.getResponse().getContentAsString();
		
		this.mockMvc.perform(post("/session/logout")
				.content(token)
				.header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON))
		.andDo(print())
		.andExpect(status().isOk())
		.andExpect(jsonPath("$", isA(String.class)));
	}
	
	@Test
	public void wrongTokenLogout() throws Exception {
		this.mockMvc.perform(post("/session/logout")
				.content(TestObjects.FAKE_TOKEN)
				.header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON))
		.andDo(print())
		.andExpect(status().isBadRequest());
	}
	
	@Test
	public void verifyValidToken() throws Exception {
		this.mockMvc.perform(post("/user/register")
				.content(TestObjects.TEST_USER)
				.header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON))
		.andDo(print())
		.andExpect(status().isOk());
		
		MvcResult result = this.mockMvc.perform(post("/session/login")
				.content(TestObjects.TEST_USER_LOGIN)
				.header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON))
		.andDo(print())
		.andExpect(status().isOk())
		.andReturn();
		
		String token = result.getResponse().getContentAsString();
		
		this.mockMvc.perform(post("/session/verify")
				.content(token)
				.header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON))
		.andDo(print())
		.andExpect(status().isOk())
		.andExpect(jsonPath("$", isA(String.class)));
	}
	
	@Test
	public void verifyWrongToken() throws Exception {
		this.mockMvc.perform(post("/session/verify")
				.content(TestObjects.FAKE_TOKEN)
				.header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON))
		.andDo(print())
		.andExpect(status().isBadRequest());
	}
}
