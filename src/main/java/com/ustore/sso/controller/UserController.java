package com.ustore.sso.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ustore.sso.model.User;
import com.ustore.sso.repo.UserRepository;

@RestController
@RequestMapping("/user")
public class UserController {

	@Autowired
	private UserRepository repository;
	
	@PostMapping("/register")
	public User createUser(@RequestBody User user) {
		User saved = this.repository.save(user);
		return saved;
	}

}
