package com.ustore.sso.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import com.ustore.sso.model.Login;
import com.ustore.sso.model.Session;
import com.ustore.sso.model.Token;
import com.ustore.sso.model.User;
import com.ustore.sso.repo.SessionRepository;
import com.ustore.sso.repo.UserRepository;

@RestController
@RequestMapping("/session")
public class SessionController {

	@Autowired
	private SessionRepository sessionRepository;
	
	@Autowired
	private UserRepository userRepository;
	
	@PostMapping("/login")
	public Token doLogin(@RequestBody Login login) {
		Example<User> ex = Example.of(login.toUser());
		
		User logged = this.userRepository.findOne(ex).orElse(null);
		
		if (logged == null) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
		}
		
		return this.sessionRepository.save(new Session(logged, null)).getLoggedToken();
	}
	
	@PostMapping("/logout")
	public String doLogout(@RequestBody Token token) {
		Session session = getSessionByToken(token);
		
		if (session == null) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
		}
		
		this.sessionRepository.delete(session);
		return "Logged out";
	}
	
	@PostMapping("/verify")
	public String doVerify(@RequestBody Token token) {
		Session session = getSessionByToken(token);
		
		if (session == null) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
		}
		return "Ok";
	}
	
	private Session getSessionByToken(Token token) {
		Example<Session> ex = Example.of(token.toSession());
		
		return this.sessionRepository.findOne(ex).orElse(null);
	}
}
