package com.ustore.sso.model;

import lombok.Data;

@Data
public class Login {

	private String email;
	private String password;
	
	public User toUser() {
		User user = new User(null, this.email, this.password);
		user.setCreatedAt(null);
		return user;
	}
}
