package com.ustore.sso.model;

import java.util.Date;
import java.util.UUID;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import lombok.Data;

@Data
@Entity
public class Session {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer id;
	
	@Column(unique = true, nullable = false)
	private String token;
	
	//@Column(nullable = false)
	@OneToOne(cascade = CascadeType.ALL, optional = false)
    @JoinColumn(name = "user_id", referencedColumnName = "id")
	private User user;
	
	@Column(nullable = false)
	private Date createdAt;
	
	private Date expirationDate;
	
	public Token getLoggedToken() {
		return new Token(this.token);
	}
	
	public Session(User user, Date expiresAt) {
		this.user = user;
		this.createdAt = new Date();
		this.expirationDate = expiresAt;
		this.token = UUID.randomUUID().toString();
	}
	
	public Session() {
		this.createdAt = new Date();
	}
}
