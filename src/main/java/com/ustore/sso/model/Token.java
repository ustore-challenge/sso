package com.ustore.sso.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Token {

	private String token;
	
	public Session toSession() {
		Session session = new Session();
		session.setToken(this.token);
		session.setCreatedAt(null);
		return session;
	}
}
