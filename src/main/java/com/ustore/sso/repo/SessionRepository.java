package com.ustore.sso.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ustore.sso.model.Session;

public interface SessionRepository extends JpaRepository<Session, Integer> {

}
