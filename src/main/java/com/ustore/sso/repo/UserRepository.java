package com.ustore.sso.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ustore.sso.model.User;

public interface UserRepository extends JpaRepository<User, Integer> {

}
